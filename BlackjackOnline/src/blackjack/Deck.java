package blackjack;

import java.util.Random;

public class Deck {
	private int DeckSize = 52;
	private int NoOfNumCards = 13;
	private int NoOfFace = 4;
	private int[][] deck = new int[NoOfNumCards][NoOfFace];
		/* deck has 52 cards and 4 different types
		 * the availability of the card will be given
		 * by a 1 or 0. Where (1=can be drawn) and
		 * (0=cannot be drawn).
		 */
	
	/**
	 * 
	 * @param NoDeck
	 * 		The number of decks which will be initialised
	 */
	public Deck() {		
		resetDeck();
		
	}
	
	/**
	 * Converts the number of the array to a card name where
	 * the numbers are in the following format:
	 * 		A			8
	 * 		2			9
	 * 		3			10
	 * 		4			J
	 * 		5			Q
	 * 		6			K
	 * 		7
	 * 
	 * And the faces are in the following order:
	 * 		Hearts
	 * 		Diamonds
	 * 		Clubs
	 * 		Spades
	 * 
	 * @param number
	 * @param face
	 * @return
	 */
	public String convertToCard(int number, int face) {
		String card = "";
		//Do the Number first
		switch(number) {
		case 0:
			card += "Ace of ";
			break;
		case 1:
			card += "2 of ";
			break;
		case 2:
			card += "3 of ";
			break;
		case 3:
			card += "4 of ";
			break;
		case 4:
			card += "5 of ";
			break;
		case 5:
			card += "6 of ";
			break;
		case 6:
			card += "7 of ";
			break;
		case 7:
			card += "8 of ";
			break;
		case 8:
			card += "9 of ";
			break;
		case 9:
			card += "10 of ";
			break;
		case 10:
			card += "Jack of ";
			break;
		case 11:
			card += "Queen of ";
			break;
		case 12:
			card += "King of ";
			break;
		default:
			System.err.println("Number2Card: Invalid Number given");
			break;
		}
		
		//Then add on the face type
		switch(face) {
		case 0:
			card += "Hearts";
			break;
		case 1:
			card += "Diamonds";
			break;
		case 2:
			card += "Clubs";
			break;
		case 3:
			card += "Spades";
			break;
		default:
			System.err.println("Number2Card: Invalid face given");
			break;
		}
		return card;
	}
	
	
	
	/**
	 * Iterate through the deck and set each card to 1
	 */
	public void resetDeck() {
		int i,j;
		for(i=0; i<NoOfFace; i++) {
			for(j=0; i<NoOfNumCards; j++) {
				deck[i][j] = 1;
			}
		}
		return;
	}
	
	
	private boolean isDeckEmpty() {
		int i,j;
		for(i=0; i<NoOfFace; i++) {
			for(j=0; i<NoOfNumCards; j++) {
				if(deck[i][j] == 1) {
					return false;
				}
			}
		}
		return true;
	}
	
	
	/**
	 * Draws a random card from the deck, if it is 
	 * empty then the deck is rescuffled.
	 * 
	 * @return
	 */
	public int[] drawCard() {
		int number=0, face=0;
		int[] card = new int[2];
		boolean valid = false;
		Random rand = new Random();
		
		//Generate a new card 
		while(valid == false) {
			number = rand.nextInt(NoOfNumCards+1);
			face = rand.nextInt(NoOfFace+1);
			
			if(deck[number][face] == 1) {
				valid = true;
			}
		}
		
		card[0] = number;
		card[1] = face;
		if(isDeckEmpty()) {		//Check if empty
			resetDeck();
			System.out.println("Re-shuffling the deck");
		}
		return card;
	}
}
