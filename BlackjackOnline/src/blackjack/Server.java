package blackjack;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

public class Server {
	// set Server parameters
	private int port = 9000;
	private Selector selector = null;
	private ServerSocketChannel serverSocketChannel = null;
	private ServerSocket serverSocket = null;
	
	private int NumberOfClients = 0;

	public Server() {
		try {
			selector = Selector.open();												// open selector
			serverSocketChannel = ServerSocketChannel.open();						// open socket channel
			serverSocket = serverSocketChannel.socket();							// set the socket associated with this channel
			serverSocketChannel.configureBlocking(false);							// set Blocking mode to non-blocking
			serverSocket.bind(new InetSocketAddress(port));							// bind port
			serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);			// registers this channel with the given selector, returning a selection key
			System.out.println("<TCPServer> Server is activated, listening on port: "+ port);

			while (selector.select() > 0) {
				for (SelectionKey key : selector.selectedKeys()) {				// test whether this key's channel is ready to accept a new socket connection
					if (key.isAcceptable()) {									// accept the connection
						ServerSocketChannel server = (ServerSocketChannel) key.channel();
						SocketChannel sc = server.accept();
						if (sc == null)
							continue;
						System.out.println("<TCPServer> Connection accepted from: " + sc.socket().getInetAddress().getHostAddress());
						NumberOfClients++;										// increase number of clients
						sc.configureBlocking(false);							// set blocking mode of the channel
						ByteBuffer buffer = ByteBuffer.allocate(1024);			// allocate buffer
						sc.register(selector, SelectionKey.OP_READ, buffer);	// set register status to READ
					}
					else if (key.isReadable()) {								// test whether this key's channel is ready for reading from Client
						ByteBuffer buffer = (ByteBuffer) key.attachment();		// get allocated buffer with size 1024
						SocketChannel sc = (SocketChannel) key.channel();
						int readBytes = 0;
						String message = null;
						
						try {													// try to read bytes from the channel into the buffer
							int ret;
							try {
								while ((ret = sc.read(buffer)) > 0)
									readBytes += ret;
							} catch (Exception e) {
								readBytes = 0;
							} finally {
								buffer.flip();
							}
							
							if (readBytes > 0) {								// finished reading, form message
								message = Charset.forName("UTF-8").decode(buffer).toString();
								buffer = null;
							}
						} finally {
							if (buffer != null)
								buffer.clear();
						}
						// react by Client's message
						if (readBytes > 0) {
							System.out.println("<TCPServer> Message received from Client" + sc.socket().getInetAddress().getHostAddress() + ": " + message);
							if ("exit".equalsIgnoreCase(message.trim())) {					// if exit, close socket channel
								System.out.println("<TCPServer> Client disconnected");
								NumberOfClients--;
								sc.close();
							} else if ("stand".equalsIgnoreCase(message.trim())) {	
								sc.register(key.selector(), SelectionKey.OP_WRITE, "Message Recieved");
							} else if ("hit".equalsIgnoreCase(message.trim())) {	
								//draw a card
								//get card data
								sc.register(key.selector(), SelectionKey.OP_WRITE, "Message Recieved");
							} else if ("double".equalsIgnoreCase(message.trim())) {	
								//draw a card
								//get card data
								//draw a card
								//get card data
								sc.register(key.selector(), SelectionKey.OP_WRITE, "Message Recieved");
							} else if ("split".equalsIgnoreCase(message.trim())) {	
								sc.register(key.selector(), SelectionKey.OP_WRITE, "Message Recieved");
							} else if ("surrender".equalsIgnoreCase(message.trim())) {	
								sc.register(key.selector(), SelectionKey.OP_WRITE, "Message Recieved");
							}
						}
					}
					// test whether this key's channel is ready for sending to Client
					else if (key.isWritable()) {
						SocketChannel sc = (SocketChannel) key.channel();
						ByteBuffer buffer = ByteBuffer.allocate(1024);
						buffer.put(((String) key.attachment()).getBytes());
						buffer.flip();
						sc.write(buffer);
						sc.register(key.selector(), SelectionKey.OP_READ, buffer);			// set register status to READ
					}
				}
				if (selector.isOpen()) {
					selector.selectedKeys().clear();
				} else {
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (serverSocketChannel != null) {
				try {
					serverSocketChannel.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) {
		new Server();
	}
}
